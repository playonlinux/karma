import chai from "chai";
import chaiHttp from "chai-http";
import app from "../../src/app";
import request from "superagent";
import knex from "../../dist/database/knex";

chai.use(chaiHttp);
chai.should();

describe("User test suite", () => {
  beforeEach(async () => {
    await knex.migrate.rollback();
    await knex.migrate.latest();
    await knex.seed.run();
  });

  afterEach(async () => {
    await knex.migrate.rollback();
  });

  it("GET /api/user exists", async () => {
    try {
      const response = await chai.request(app).get("/api/user");
      response.should.have.property("status").and.equal(200);
    } catch (error) {
      throw error;
    }
  });

  it("GET /api/user/:id/account returns user (:userId) balance + account operations list", async () => {
    try {
      const response: request.Response = await chai
        .request(app)
        .get("/api/user/1/account");

      response.should.have.property("status").and.equal(200);

      response.body.should.have.property("balance");
      response.body.balance.should.be.a("number");
      response.body.balance.should.equal(3800);

      response.body.should.have.property("operations");
      response.body.operations.should.be.an("array");
    } catch (error) {
      throw error;
    }
  });
});
