// Update with your config settings.

module.exports = {
  development: {
    client: "pg",
    connection: {
      host: "127.0.0.1",
      database: "karma-test",
      user: "karma-user",
      password: "karma-user"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      extension: "ts",
      directory: __dirname + "/src/database/migrations",
      tableName: "knex_migrations"
    },
    seeds: {
      extension: "ts",
      directory: __dirname + "/src/database/seeds"
    }
  },

  staging: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  production: {
    client: "postgresql",
    connection: {
      database: "my_db",
      user: "username",
      password: "password"
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }
};
