import * as bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import logger from "morgan";

import UserApi from "./user/user.api";
// import CompilerApi from "./routes/compiler.api";
// import DeviceModelApi from "./routes/device-model.api";
// import DeviceApi from "./routes/device.api";
// import LabelApi from "./routes/label.api";
// import MessageApi from "./routes/message.api";
// import NotifierApi from "./routes/notifier.api";
// import PayloadApi from "./routes/payload.api";
// import RuleApi from "./routes/rule.api";
// import SensorApi from "./routes/sensor.api";
// import TriggerApi from "./routes/trigger.api";
// import UnitApi from "./routes/unit.api";
// import WebhookApi from "./routes/webhook.api";

class App {
  public express: express.Application;

  constructor() {
    this.express = express();

    // Enable CORS
    this.express.use(cors());

    this.middleware();
    this.routes();
  }

  private middleware(): void {
    this.express.use(logger("dev"));
    this.express.use(
      bodyParser.json({ limit: "15mb", type: "application/json" })
    );
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes(): void {
    this.express.use("/api/user", UserApi);
  }
}

export default new App().express;
