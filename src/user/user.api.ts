import { NextFunction } from "connect";
import { Request, Response, Router } from "express";
import client from "../database/connection";
import { QueryResult } from "pg";

class UserApi {
  public router: Router;

  constructor() {
    this.router = Router();

    this.middlewares();
    this.routes();
  }

  private middlewares() {
    this.router.use(
      "/:id/*",
      async (request: Request, response: Response, next: NextFunction) => {
        try {
          const results: QueryResult = await client.query({
            text: "SELECT * FROM users WHERE id=$1::integer",
            values: [request.params.id]
          });

          request.body.user = results.rows[0];
          if (!request.body.user) {
            throw { code: 404, message: "User not found" };
          }
          next();
        } catch (error) {
          if (error.code) {
            response.status(error.code).send(error.message);
          } else {
            response.status(500).send(error);
          }
        }
      }
    );
  }
  private routes() {
    this.router.get("/", (request: Request, response: Response) =>
      response.status(200).end()
    );

    this.router.get("/:id/account", (request: Request, response: Response) =>
      response.status(200).json({ balance: 0, operations: [] })
    );
  }
}

const userApi = new UserApi();

export default userApi.router;
