import { Client } from "pg";

const client = new Client({
  host: "127.0.0.1",
  database: "karma-test",
  user: "karma-user",
  password: "karma-user"
});

(async () => await client.connect())();

export default client;
