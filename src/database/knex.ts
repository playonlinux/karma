const environment = process.env.NODE_ENV || "development";
const config = require("../../knexfile")[environment];
import knex from "knex";

export default knex(config);
