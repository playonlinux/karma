import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  return knex.schema.createTable("users", table => {
    table.increments();
    table.string("first_name").nullable();
    table.string("last_name").nullable();
    table
      .string("email")
      .notNullable()
      .unique();
    table.integer("azure_id").nullable();
    table.dateTime("created_at").defaultTo(knex.fn.now());
    table.dateTime("updated_at").nullable();
    table.dateTime("deleted_at").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable("users");
}
